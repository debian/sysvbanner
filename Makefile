CFLAGS=-Wall -O2

banner: sysvbanner.c
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	$(RM) banner
