=pod

=encoding UTF-8

=head1 NAME

banner - print large banner

=head1 SYNOPSIS

B<banner> I<text> [I<text> [...]]

=head1 DESCRIPTION

B<banner> prints out the first 10 characters of text in large letters.

Multiple text are output on multiple large lines.

=head1 LIMITATIONS

The character set which can be used in text is limited to ASCII and
other characters will be rendered as whitespace.

=head1 SEE ALSO

B<toilet>(1),
B<figlet>(6).

=head1 AUTHORS

The original author of this implementation is Brian Wallis, with further
improvements by David Frey, which also packaged it for Debian.

The manual page was updated an rewritten in POD format by Ricardo Mones.

=cut
